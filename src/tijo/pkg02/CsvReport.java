/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tijo.pkg02;

import java.util.List;

/**
 *
 * @author student
 */
public interface CsvReport {
    /** funkcja tworzy plik CSV oraz zapisuje go w okreslonej lokalizacji w systemie operacyjnym
    *
    * @param linie w odpowiednim formacie do zapisu
    * @return <b>true</b> w przypadku pomyslnego zapisu pliku, w przeciwnym wypadku zwraca <b>false</b>
    * */
    boolean saveCsvReport(List<String> lines);
}
