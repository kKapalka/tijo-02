/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tijo.pkg02;

/**
 *
 * @author student
 */
public class Main {
    public static void main(String[] args) {
        final String HEADER = "LP;DRUZYNA;MECZE;PUNKTY;ZWYCIESTWA;REMISY;PORAZKI;BRAMKI;ROZNICA;";
        final String CONNECTION_BY = ";";
        // prosze zmodyfikowac sciezke aby prawidlowo odczytac plik w wlasnym systemie operacyjnym
        final String CSV_FILE = "/Users/tomaszgadek/Downloads/serieA.csv";
        List<Team> teams = GenerateTeamStatistics.getTeams();
        ListOfTeamToLineConverter listOfTeamToLineConverter = new ListOfTeamToLineConverterImpl();
        List<String> linesOfTeams = listOfTeamToLineConverter.toLines(teams, CONNECTION_BY, HEADER);
        CsvReport csvReport = new SaveCsvFile(CSV_FILE);
        boolean isSaved = csvReport.saveCsvReport(linesOfTeams);
        if(isSaved) {
            System.out.println("Plik " + CSV_FILE + " zostal zapisany");
        } else {
            System.out.println("Blad podczas zapisu pliku!");
        }
    }
}
