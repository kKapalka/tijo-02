/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tijo.pkg02;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author student
 */
public class GenerateTeamStatistics {
    /** pobierz druzyny w wloskiej serie A */
    public static List<Team> getTeams() {
    List<Team> listOfTeams = new ArrayList<>();
    listOfTeams.add(new Team(1, "SSC Napoli", 66, 25, 21, 3, 1, "55-15"));
    listOfTeams.add(new Team(2, "Juventus Turyn", 65, 25, 21, 2, 2, "62-15"));
    listOfTeams.add(new Team(3, "AS Roma", 50, 25, 15, 5, 5, "40-19"));
    listOfTeams.add(new Team(4, "Lazio Rzym", 49, 25, 15, 4, 6, "60-33"));
    listOfTeams.add(new Team(5, "Inter Mediolan", 48, 25, 13, 9, 3, "40-21"));
    listOfTeams.add(new Team(6, "Sampdoria Genoa", 41, 25, 12, 5, 8, "44-33"));
    listOfTeams.add(new Team(7, "AC Milan", 41, 25, 12, 5, 8, "35-30"));
    listOfTeams.add(new Team(8, "Atalanta Bergamo", 38, 25, 10, 8, 7, "37-29"));
    listOfTeams.add(new Team(9, "Torino", 36, 25, 8, 12, 5, "35-30"));
    listOfTeams.add(new Team(10, "Udinese Calcio", 33, 25, 10, 3, 12, "36-35"));
    listOfTeams.add(new Team(11, "ACF Fiorentina", 32, 25, 8, 8, 9, "34-32"));
    listOfTeams.add(new Team(12, "Genoa", 30, 25, 8, 6, 11, "21-25"));
    listOfTeams.add(new Team(13, "Bologna FC", 30, 25, 9, 3, 13, "31-38"));
    listOfTeams.add(new Team(14, "Cagliari", 25, 25, 7, 4, 14, "23-36"));
    listOfTeams.add(new Team(15, "Chievo Werona", 25, 25, 6, 7, 12, "23-42"));
    listOfTeams.add(new Team(16, "US Sassuolo Calcio", 23, 25, 6, 5, 14, "15-43"));
    listOfTeams.add(new Team(17, "FC Crotone", 21, 25, 5, 6, 14, "21-44"));
    listOfTeams.add(new Team(18, "SPAL", 17, 25, 3, 8, 14, "23-47"));
    listOfTeams.add(new Team(19, "Hellas Verona", 16, 25, 4, 4, 17, "22-50"));
    listOfTeams.add(new Team(20, "Benevento Calcio", 10, 25, 3, 1, 21, "18-58"));
    return listOfTeams;
    }
    }
