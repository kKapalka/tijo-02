/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tijo.pkg02;

import java.util.List;

/**
 *
 * @author student
 */
public interface ListOfTeamToLineConverter {
    /** Wykonuje konwersje listy obiektow typu Team do listy linii wygenerowanych w odpowiednim formacie */
    List<String> toLines(List<Team> teams, String connectionBy, String header);
}
